package WordChain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String end="";
        do {
        Differs differs = new Differs();
        ReadFromFile readFromFile = new ReadFromFile();
        Scanner scanner = new Scanner(System.in);

        System.out.print("If you want to load words from file, just press  \"f\" \nor press any key to use console. ->   ");
        String fromfile = scanner.nextLine();
        String input = "";
            // Elágazás hogy honnan jöjjenek a szavak
            if(fromfile.equals("f")){
                input = readFromFile.ReadFile();
                System.out.print("\nWords from file: ");
                System.out.println(input);

            }else {// Adatok beolvasása konzolról
                System.out.println(" ");
                System.out.print("Please enter the words separated by space: ");
                input = scanner.nextLine();

            }
            String[] words = input.split(" ");
            Arrays.sort(words);

            // Beérkezett szavak, ha megfelelnek ArrayListbe kerülnek
            ArrayList<String> chained = new ArrayList<String>();
            int startingPoint = 0;
            boolean done = false;

            chained.add(words[startingPoint]);
            String word1 = words[startingPoint];
            //String word1="";
            boolean nextWordFound = false;
            do {
                for (int i = 0; i < words.length; i++) {
                    if (differs.differsByOne(word1, words[i]) && !chained.contains(words[i])) {
                        chained.add(words[i]);
                        word1 = words[i];
                        nextWordFound = true;
                        break;
                    }
                }
                if (chained.size() == words.length) { //Ha a ArrayList és az Array mérete megegyezik akkor minden elem felhasználásra került
                    done = true;
                } else { // Ha kimarad egy elem is akkor nem hozható léte szólánc -> amit ki is íratunk
                    if (!nextWordFound && startingPoint != words.length - 1) {
                        startingPoint++;
                        chained.clear();
                        word1 = words[startingPoint];
                        chained.add(word1);
                        nextWordFound = false;
                    }
                    if (!nextWordFound && (startingPoint == words.length - 1)) {
                        System.out.println("Sorry can't make word chain");
                        System.exit(0);
                    }
                }
                nextWordFound = false;
            } while (!done); //Ha létrehozható "done" igaz lesz így kilép a ciklusból és kiírja az eredményt

            System.out.print("Output:  ");
            for (String word : chained) {
                System.out.print(word + " ");

            }
            System.out.println(" ");
            System.out.print("\nDo you want it again? Just press: \"y\" ->   ");
            end = scanner.nextLine();
        }while(end.equals("y"));

        System.exit(0);
    }
}




