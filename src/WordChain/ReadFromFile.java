package WordChain;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFile{

    /** Adatok beolvasása file-ból előre meghatározott útvonalról (C:\Test\test.txt) */
    protected String ReadFile(){

    String fileName = "C:\\Test\\test.txt";
    String content ="";
    try {
    BufferedReader reader = new BufferedReader(new FileReader(fileName));
    StringBuilder stringBuilder = new StringBuilder();
    String line = null;
    //String ls = System.getProperty(" ");
    while ((line = reader.readLine()) != null) {
        stringBuilder.append(line);
       // stringBuilder.append(ls);
    }
   // utolsó line separator törlése de nem is kell
   /* stringBuilder.deleteCharAt(stringBuilder.length() - 1);*/
    reader.close();

    content = stringBuilder.toString();
    }catch (FileNotFoundException e){System.out.println("File not found");}
    catch (IOException e) {
        e.printStackTrace();
    }
        return content;
}
}
