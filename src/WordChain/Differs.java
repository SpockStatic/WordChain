package WordChain;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Differs{
    /** Megnézi a szavak hosszát és a szavakban lévő karaktereket összehasonlítja */
    protected boolean differsByOne(String word1, String word2){
        int count = 0;
        if (word1.equals(word2))
        {
            return false;  }

        int word1Length = word1.length();
        int word2Length = word2.length();
        int longerWord = Math.max(word1Length,word2Length);
        int smallerWord = Math.min(word1Length,word2Length);

        //Egyforma hosszúságú
        if(word1Length == word2Length){
            for (int i = 0; i < word1.length(); i++)
            {
                if  (word1.charAt(i) != word2.charAt(i)){
                    count++;  }
                if  (count > 1)
                {
                    return false;  }
            }
            return true;}

        //Különböző hosszúságú
        if(word1Length != word2Length){
                 for (int i = 0; i < smallerWord; i++){
                 if (word1.charAt(i) != word2.charAt(i)) {
                        count++;
                    }

                if  (count > 2){
                   return false;}
            }
            return true;
        }



        return true;
    }
}
